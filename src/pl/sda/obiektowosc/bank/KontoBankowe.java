package pl.sda.obiektowosc.bank;

public class KontoBankowe {

    private long numerKonta; //było public
    private int stanKonta; //było public

    public KontoBankowe(long numerKonta, int stanKonta) {
        this.numerKonta = numerKonta;
        this.stanKonta = stanKonta;
    }

    // Alt Insert  +fn   --> stworzenie gettera  lub settera
    public long getNumerKonta() {
        return numerKonta;
    }

    public void wyswietlStanKonta() {
        System.out.println("Stan konta #" + numerKonta + "wynosi : " + stanKonta);
    }

    public void wplacSrdoki(int kwota) {
        stanKonta += kwota;
    }

    public int pobierzSrodki(int kwota) {
        if (kwota > stanKonta) {
            return 0;
        } else {
            stanKonta -= kwota;
            return kwota;

        }
    }
}
