package pl.sda.obiektowosc.bank;

public class Main {
    public static void main(String[] args) {
        KontoBankowe kontoAndrzeja = new KontoBankowe(11, 1000);
        KontoBankowe kontoBeaty = new KontoBankowe(100, 1000);

        System.out.println("Przed przelewem");
        kontoAndrzeja.wyswietlStanKonta();
        kontoBeaty.wyswietlStanKonta();

        int pobranaKwota = kontoBeaty.pobierzSrodki(100);
        kontoAndrzeja.wplacSrdoki(pobranaKwota);

        kontoAndrzeja.wplacSrdoki(kontoBeaty.pobierzSrodki(10000));

        System.out.println("Po przelewie");
        kontoAndrzeja.wyswietlStanKonta();
        kontoBeaty.wyswietlStanKonta();

        KontoBankowe kontoCwaniaczka = new KontoBankowe(123, 0);
        kontoCwaniaczka.wyswietlStanKonta();

        System.out.println("Czary mary");
  //      kontoCwaniaczka.stanKonta = 100000;
        kontoCwaniaczka.wyswietlStanKonta();
    }
}
