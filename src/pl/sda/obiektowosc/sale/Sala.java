package pl.sda.obiektowosc.sale;

public class Sala {  //konstruktor Sala

    String nazwa; //null
    double iloscM2; //0.0
    int liczbaStanowsik; // 0
    boolean czyJestRzutnik; //false
    boolean czyJestWolna = true; //false jeśli nie wpisano = true

    void wyswietlOpisSali() {
        String opis = String.format("Sala %s o pow. %.2f,, " + "liczba stanowisk: %d", nazwa, iloscM2, liczbaStanowsik);
        if (czyJestRzutnik) {
            opis += ", sala posiada rzutnik";
        } else {
            opis += ", sala nie posiada rzutnika";
        }
        System.out.println(opis);
    }

    public Sala(String nazwa, double iloscM2, int liczbaStanowsik, boolean czyJestRzutnik) {
        this.nazwa = nazwa;
        this.iloscM2 = iloscM2;
        this.liczbaStanowsik = liczbaStanowsik;
        this.czyJestRzutnik = czyJestRzutnik;


    }

    public Sala() {
    }

}
