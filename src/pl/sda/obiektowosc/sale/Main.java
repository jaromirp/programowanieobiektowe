package pl.sda.obiektowosc.sale;

public class Main {
    public static void main(String[] args) {

        Sala gdansk = new Sala ();
        gdansk.nazwa = "Gdansk";
        gdansk.iloscM2 = 50.05;
        gdansk.liczbaStanowsik = 8;
        gdansk.czyJestRzutnik = true;

        Sala sztukaW = new Sala ();
        sztukaW.nazwa = "Java";
        sztukaW.iloscM2 = 45;
        sztukaW.liczbaStanowsik = 12;
        sztukaW.czyJestRzutnik = false;

        Sala nowa = new Sala ("Nowa", 100.02, 30, false);


        Sala[] zarzadzaneSale = new Sala[] {gdansk, sztukaW, nowa};

        Menadzer jan = new Menadzer();
        jan.imie = "Jan";
        jan.sale = zarzadzaneSale;
        jan.wyswietlDostepneSale();

        jan.zablokujSale("Java");
        jan.wyswietlDostepneSale();
    }
}
